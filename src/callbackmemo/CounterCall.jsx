import React,{useState,useCallback} from 'react'
import Button from './Button'
const CounterCall=()=>
{

    const [count,setCount]=useState(0)

    const counterHandle=useCallback(
        (e) => {
            setCount(Math.floor(Math.random()*10))
            console.log(`Cliked ${count}`)
        },
        []
    )

    const [text,setTxt]=useState(0)

    const showText=useCallback(()=>{
        setTxt(t=>t+1)
    },[setTxt])

    // const counterHandle=
    // () => {
    //     setCount(count+1)
    //     console.log(count)
    // }
      

    return(
        <div>
        {text}
            <Button showText={showText}></Button>
        </div>
    )
}
export default CounterCall