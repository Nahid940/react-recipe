import React,{useState} from 'react'

import Memo from './Memo'

function MemoProducer() {

    const [value,setValue]=useState(0)

    const increment=()=>{
        setValue(1)
    }

    return (
        <div>
            <button>Click Me</button>
            <Memo value={value} increment={increment}/>
        </div>
    )
}

export default MemoProducer
