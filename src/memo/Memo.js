import React from 'react'

function Memo({value,increment}) {
    console.log("Rendering value"+value)

    const handleClick=()=>{
        increment()
    }

    return (
        <div>

            {value}

            <button onClick={handleClick}>Click me from memo</button>
            
        </div>
    )
}

export default React.memo(Memo)
