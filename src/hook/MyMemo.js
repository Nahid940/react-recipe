import React,{useState,useCallback} from 'react'

const randomColour = () => '#'+(Math.random()*0xFFFFFF<<0).toString(16);

const MyMemo = React.memo((props) => 
    <button onClick={props.onClick} style={{background: randomColour()}}> 
        {props.children}
    </button>
)

export default MyMemo