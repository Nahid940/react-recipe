import React, { useState,useCallback } from 'react';
import MyMemo from './MyMemo'

const MemoUser = () => {
  const [delta, setDelta] = useState(1);
  const [c, setC] = useState(0);

  const incrementDelta = useCallback(
    () => setDelta(delta => delta + 1),
    []
  );

  const increment =useCallback(
    () => 
      setC(c => c + delta),
    [delta]
  );

  // const [cx, setCX] = useState(0);

  // const handleC=useMemo(() =>{ setCX(cx=>cx+delta) }, [],console.log(cx))


  return (<div>
    <div> Delta is {delta} </div>
    <div> Counter is {c} </div>
    <br/>
        <div>
            <MyMemo onClick={incrementDelta} delta={delta}>Increment Delta</MyMemo>
            <MyMemo onClick={increment} c={c}>Increment Counter</MyMemo>

            {/* <button onClick={handleC}>From Parent {cx}</button> */}
        </div>
    <br/>
    <div> Newly Created Functions: </div>
  </div>)
}

export default MemoUser

