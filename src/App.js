import React,{useEffect,useState,useCallback} from 'react';
import logo from './logo.svg';
import './App.css';
import Recipes from './component/Recipes'
import UseEffect from './useeffect/UseEffect';
import MyItem from './useeffect/MyItem';
import Example from './useeffect/Test';
import Memo from './memo/Memo';
import MemoProducer from './memo/MemoProducer';
import Hello from './callback/Hello';
import Numbers from './callback/Numbers';
import MemoUser from './hook/MemoUser';
import Form from './refs/Form';
import CounterCall from './callbackmemo/CounterCall';

function App() {
  const APP_ID="db12307d"
  const APP_KEY="db2dead616749758c5cbe27255443dd7"


  const [recipes,setRecipes]=useState([])

  const [search,setSearch]=useState('')

  const [query,setquery]=useState('chicken')
 

  useEffect(()=>{
    // getRecipe()
  },[query])

  const getRecipe=async ()=>{
      const response=await fetch(`https://api.edamam.com/search?q=${query}&app_id=${APP_ID}&app_key=${APP_KEY}`)

      const data=await response.json()
      setRecipes(data.hits)
  }

  const updateSearch=(event)=>{
      setSearch(
        event.target.value
      )
  }

  const doSearch=(event)=>{
    event.preventDefault()
    setquery(search)
  }

  //Use callback======================
    const [count,setCount]=useState(0)
    // const increment=()=>{
    //   setCount(count+1)
    // }

    const increment=useCallback((n)=>{
      setCount(x=>x+n)
    },[setCount])


    const [favouriteNumber,setFavouriteNumber]=useState([3,9,12,16])
  //=============================

  return (
    <div className="App">

      <form className="search-form" onSubmit={doSearch}>
          <input type="text" className="search-bar" value={search} onChange={updateSearch} />
          <button  className="search-button" type="submit">Search</button>
      </form>
      
      <div className="recipe">
        {recipes.map(rcp=>(
          <Recipes key={Math.random()*1000}
          title={rcp.recipe.label} 
          calories={rcp.recipe.calories} 
          image={rcp.recipe.image}
          ingredients={rcp.recipe.ingredients}
          />
        ))}

        {/* <UseEffect></UseEffect> */}
        {/* <MyItem/> */}
        {/* <Example/> */}
        {/* <MemoProducer/> */}

        {/* <Hello increment={increment}/>
        Count {count} */}

        {/* {favouriteNumber.map(fv=>(
          <Numbers increment={increment} n={fv}/>
        ))} */}

        {/* <MemoUser/> */}
        {/* <Form/> */}

        <CounterCall/>

      </div>
      
    </div>
  );
}

export default App;
