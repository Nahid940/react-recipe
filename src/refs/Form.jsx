import React,{useRef,useEffect} from 'react'

const Form =()=>
{
    const name=useRef()
    const lastname=useRef()
    const age=useRef()

    const handleSubmit=(event)=>
    {
        event.preventDefault()
        // console.log(event.current.value)
    }

    const focus=()=>{name.current.focus()}

    useEffect(()=>{console.log(name)})

    const handleKeyUp=(event)=>
    {
        // console.log(event.target.name)
        
        if(event.keyCode===13)
        {
            switch(event.target.name){
                case 'name':
                lastname.current.focus();
                break;
                case 'lastname':
                age.current.focus();
            };
        }
    }

   
    return (
        <div>
            <form onSubmit={handleSubmit}>
                <span>Name</span>
                <input type="text" onKeyUp={handleKeyUp} name="name" ref={name}/>
                <br/>
                <span>Last name</span>
                <input type="text" onKeyUp={handleKeyUp } name="lastname" ref={lastname}/>
                <br/>
                <span>Age</span>
                <input type="text" onKeyUp={handleKeyUp.bind(this,'age')} name="age" ref={age}/>

                <button type="submit" >Submit</button>

            </form>
        </div>
    )
}

export default Form