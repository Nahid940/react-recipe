import React,{useRef} from 'react'
import {UseCountRenders} from './UseCountRenders'

const Numbers =({n,increment})=>{

    const incrementHandle=()=>{
        increment(n)
    }

    UseCountRenders()
    return(
        <div>
            <button onClick={incrementHandle}>Click me</button>
        </div>
    )
}

export default React.memo(Numbers)