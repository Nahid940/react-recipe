import React, { Component } from 'react'
import FriendStatus from './FriendStatus'

class ChatAPI extends Component {

    constructor(props)
    {
        super(props)
        this.state={
            friend:{
                id:5
            }
        }
    }
    render() {
        return (
            <div>

                <FriendStatus friend={this.state.friend}/>
                
            </div>
        )
    }
}

export default ChatAPI
