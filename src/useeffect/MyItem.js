import React, { Component } from 'react'

class MyItem extends Component {


    constructor(props)
    {
        super(props)
        this.state={
            count:0
        }
    }

    componentDidMount()
    {
        console.log("The count value is "+this.state.count)
    }

    componentDidUpdate()
    {
        console.log("The update value is "+this.state.count)
    }



    render() {
        return (

            <div>

                <button onClick={()=>{this.setState({count:this.state.count+1})}}>Click</button>
                
            </div>
        )
    }
}

export default MyItem
