import React,{useState,useEffect} from 'react'

const UseEffect=()=>
{
    const [count,setCount]=useState(0)

  
    useEffect(()=>{
        // handleState()
        console.log(count)
    },[count])
    
    const handleState=()=>{
        setCount(count+1)
    }

    return(
        <div>
           <button onClick={handleState}>Click me</button>
           {count}
        </div>
    )
}


export default UseEffect